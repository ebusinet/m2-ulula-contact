<?php
/**
 * @author Ulula Team
 * @copyright Copyright (c) 2021 Ulula (https://www.ulula.net)
 * @package Ulula_Contact
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Ulula_Contact', __DIR__);
